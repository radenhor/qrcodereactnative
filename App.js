/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
'use strict';
import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
  Alert
} from 'react-native';
import { RNCamera } from 'react-native-camera';
import QRCode from 'react-native-qrcode-svg';
class App extends React.Component{
  state = {
    text: 'http://facebook.github.io/react-native/',
    isScan : true,
    data : []
  };
  onBarCodeRead = e => {
    
    if(this.state.isScan){
      let url = e.data;
      this.setState({isScan:false},()=>{
        fetch(url).then(res=>res.json()).then(res=>{console.log(res.DATA[0]);this.setState({isScan:false,data:res.DATA[0]})}).catch(err=>{
          Alert.alert(
            'Alert Title',
            'My Alert Msg',
            [
              {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
              {
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel',
              },
              {text: 'OK', onPress: () =>  this.setState({isScan:true})},
            ],
            {cancelable: false},
          );
         
        })
      })
    }
    
    
    // Alert.alert('Scaned')
  };
  render(){
    return(
      <SafeAreaView style={{flex: 1,
        flexDirection: 'column',
        backgroundColor: 'white',}}>
           
        {/* <QRCode
            value="http://awesome.link.qr"
        /> */}
        {this.state.isScan ? 
        <RNCamera
        style = {{flex: 1,
         padding :30,
         justifyContent: 'flex-end',
         alignItems: 'center'}}
         barCodeTypes={[RNCamera.Constants.BarCodeType.qr]}
         flashMode={RNCamera.Constants.FlashMode.on}
         // style={styles.preview}
         onBarCodeRead={this.onBarCodeRead}
        //  onGoogleVisionBarcodesDetected = {(b)=>{console.log(b)}}
         ref={cam => (this.camera = cam)}
       ></RNCamera>
       :
  <Text>{this.state.data.TITLE}</Text>
        }
         
      </SafeAreaView>
      
    )
  }
}
export default App;
